library verilog;
use verilog.vl_types.all;
entity alu is
    port(
        add_sub         : in     vl_logic;
        set_low         : in     vl_logic;
        set_high        : in     vl_logic;
        operanda        : in     vl_logic_vector(7 downto 0);
        operandb        : in     vl_logic_vector(7 downto 0);
        result          : out    vl_logic_vector(7 downto 0)
    );
end alu;
