transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/regfile.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/delay_counter.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/pc.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/datapath.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/alu.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/op1_mux.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/op2_mux.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/temp_register.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/immediate_extractor.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/write_address_select.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/branch_logic.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/decoder.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/result_mux.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/instruction_rom.v}
vlog -vlog01compat -work work +incdir+C:/Users/Nicholas/Desktop/Lab5/lab5 {C:/Users/Nicholas/Desktop/Lab5/lab5/stepper_rom.v}

