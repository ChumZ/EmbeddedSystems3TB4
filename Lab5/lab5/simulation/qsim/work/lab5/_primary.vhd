library verilog;
use verilog.vl_types.all;
entity lab5 is
    port(
        altera_reserved_tms: in     vl_logic;
        altera_reserved_tck: in     vl_logic;
        altera_reserved_tdi: in     vl_logic;
        altera_reserved_tdo: out    vl_logic;
        clk             : in     vl_logic;
        reset_n         : in     vl_logic;
        stepper_signals : out    vl_logic_vector(3 downto 0);
        LEDG            : out    vl_logic_vector(3 downto 0)
    );
end lab5;
