library verilog;
use verilog.vl_types.all;
entity pc_vlg_sample_tst is
    port(
        branch          : in     vl_logic;
        clk             : in     vl_logic;
        increment       : in     vl_logic;
        newpc           : in     vl_logic_vector(7 downto 0);
        reset_n         : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end pc_vlg_sample_tst;
