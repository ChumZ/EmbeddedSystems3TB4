library verilog;
use verilog.vl_types.all;
entity result_mux_vlg_sample_tst is
    port(
        alu_result      : in     vl_logic_vector(7 downto 0);
        select_result   : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end result_mux_vlg_sample_tst;
