library verilog;
use verilog.vl_types.all;
entity pc is
    port(
        clk             : in     vl_logic;
        reset_n         : in     vl_logic;
        branch          : in     vl_logic;
        increment       : in     vl_logic;
        newpc           : in     vl_logic_vector(7 downto 0);
        pc              : out    vl_logic_vector(7 downto 0)
    );
end pc;
