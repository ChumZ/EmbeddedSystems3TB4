library verilog;
use verilog.vl_types.all;
entity control_fsm_vlg_sample_tst is
    port(
        addi            : in     vl_logic;
        br              : in     vl_logic;
        brz             : in     vl_logic;
        clk             : in     vl_logic;
        clr             : in     vl_logic;
        delay_done      : in     vl_logic;
        mov             : in     vl_logic;
        mova            : in     vl_logic;
        movr            : in     vl_logic;
        movrhs          : in     vl_logic;
        pause           : in     vl_logic;
        register0_is_zero: in     vl_logic;
        reset_n         : in     vl_logic;
        sr0             : in     vl_logic;
        srh0            : in     vl_logic;
        subi            : in     vl_logic;
        temp_is_negative: in     vl_logic;
        temp_is_positive: in     vl_logic;
        temp_is_zero    : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end control_fsm_vlg_sample_tst;
