library verilog;
use verilog.vl_types.all;
entity result_mux is
    port(
        select_result   : in     vl_logic;
        alu_result      : in     vl_logic_vector(7 downto 0);
        result          : out    vl_logic_vector(7 downto 0)
    );
end result_mux;
