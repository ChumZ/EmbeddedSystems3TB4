library verilog;
use verilog.vl_types.all;
entity decoder_vlg_check_tst is
    port(
        addi            : in     vl_logic;
        br              : in     vl_logic;
        brz             : in     vl_logic;
        clr             : in     vl_logic;
        mov             : in     vl_logic;
        mova            : in     vl_logic;
        movr            : in     vl_logic;
        movrhs          : in     vl_logic;
        pause           : in     vl_logic;
        sr0             : in     vl_logic;
        srh0            : in     vl_logic;
        subi            : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end decoder_vlg_check_tst;
