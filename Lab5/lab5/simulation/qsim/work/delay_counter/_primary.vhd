library verilog;
use verilog.vl_types.all;
entity delay_counter is
    port(
        altera_reserved_tms: in     vl_logic;
        altera_reserved_tck: in     vl_logic;
        altera_reserved_tdi: in     vl_logic;
        altera_reserved_tdo: out    vl_logic;
        clk             : in     vl_logic;
        reset_n         : in     vl_logic;
        start           : in     vl_logic;
        enable          : in     vl_logic;
        delay           : in     vl_logic_vector(7 downto 0);
        done            : out    vl_logic
    );
end delay_counter;
