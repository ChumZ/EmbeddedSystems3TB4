library verilog;
use verilog.vl_types.all;
entity decoder is
    port(
        instruction     : in     vl_logic_vector(7 downto 2);
        br              : out    vl_logic;
        brz             : out    vl_logic;
        addi            : out    vl_logic;
        subi            : out    vl_logic;
        sr0             : out    vl_logic;
        srh0            : out    vl_logic;
        clr             : out    vl_logic;
        mov             : out    vl_logic;
        mova            : out    vl_logic;
        movr            : out    vl_logic;
        movrhs          : out    vl_logic;
        pause           : out    vl_logic
    );
end decoder;
