library verilog;
use verilog.vl_types.all;
entity branch_logic_vlg_check_tst is
    port(
        branch          : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end branch_logic_vlg_check_tst;
