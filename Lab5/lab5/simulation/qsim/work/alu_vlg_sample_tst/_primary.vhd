library verilog;
use verilog.vl_types.all;
entity alu_vlg_sample_tst is
    port(
        add_sub         : in     vl_logic;
        operanda        : in     vl_logic_vector(7 downto 0);
        operandb        : in     vl_logic_vector(7 downto 0);
        set_high        : in     vl_logic;
        set_low         : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end alu_vlg_sample_tst;
