library verilog;
use verilog.vl_types.all;
entity delay_counter_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        delay           : in     vl_logic_vector(7 downto 0);
        enable          : in     vl_logic;
        reset_n         : in     vl_logic;
        start           : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end delay_counter_vlg_sample_tst;
