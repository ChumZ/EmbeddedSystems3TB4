library verilog;
use verilog.vl_types.all;
entity branch_logic is
    port(
        register0       : in     vl_logic_vector(7 downto 0);
        branch          : out    vl_logic
    );
end branch_logic;
