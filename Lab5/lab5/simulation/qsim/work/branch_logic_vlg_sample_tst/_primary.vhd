library verilog;
use verilog.vl_types.all;
entity branch_logic_vlg_sample_tst is
    port(
        register0       : in     vl_logic_vector(7 downto 0);
        sampler_tx      : out    vl_logic
    );
end branch_logic_vlg_sample_tst;
