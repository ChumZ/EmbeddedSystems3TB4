library verilog;
use verilog.vl_types.all;
entity datapath_vlg_sample_tst is
    port(
        alu_add_sub     : in     vl_logic;
        alu_set_high    : in     vl_logic;
        alu_set_low     : in     vl_logic;
        clk             : in     vl_logic;
        commit_branch   : in     vl_logic;
        decrement_temp  : in     vl_logic;
        enable_delay_counter: in     vl_logic;
        increment_pc    : in     vl_logic;
        increment_temp  : in     vl_logic;
        load_temp       : in     vl_logic;
        op1_mux_select  : in     vl_logic_vector(1 downto 0);
        op2_mux_select  : in     vl_logic_vector(1 downto 0);
        reset_n         : in     vl_logic;
        result_mux_select: in     vl_logic;
        select_immediate: in     vl_logic_vector(1 downto 0);
        select_write_address: in     vl_logic_vector(1 downto 0);
        start_delay_counter: in     vl_logic;
        write_reg_file  : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end datapath_vlg_sample_tst;
