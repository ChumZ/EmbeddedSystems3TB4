module delay_counter (input clk, reset_n, start, enable, input [7:0] delay, output done);
parameter BASIC_PERIOD=20'd1000000;//500000;  

reg[7:0] downcounter;
reg[19:0] timer;

assign done = (downcounter==8'b00000000)?1'b1:1'b0;

always @(posedge clk)
begin
		if(!reset_n)
		begin
			timer<=20'd0;
			downcounter<=8'h00;
		end
		else if(start==1'b1)
		begin
			timer<=20'd0;
			downcounter<=delay;
		end
		else if(enable==1'b1)
		begin
			if(timer<(BASIC_PERIOD-20'd1))
			begin
				timer<=timer+16'd1;
			end
			else if(downcounter != 0) 
			begin
				downcounter<=downcounter-8'b1;
				timer<=20'd0;
			end
		end
end
endmodule
