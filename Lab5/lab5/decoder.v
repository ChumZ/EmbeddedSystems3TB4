module decoder (input [7:2] instruction,
				output br, brz, addi, subi, sr0, srh0, clr, mov, mova, movr, movrhs, pause );

assign br = (instruction[7:5] == 3'b100) ? 1'b1: 1'b0;
assign clr = (instruction[7:2] == 6'b011000) ? 1'b1: 1'b0;
assign brz = (instruction[7:5] == 3'b101) ? 1'b1: 1'b0;
assign addi = (instruction[7:5] == 3'b000) ? 1'b1: 1'b0;
assign subi = (instruction[7:5] == 3'b001) ? 1'b1: 1'b0;
assign sr0 = (instruction[7:4] == 4'b0100) ? 1'b1: 1'b0;
assign srh0 = (instruction[7:4] == 4'b0101) ? 1'b1: 1'b0;
assign mov = (instruction[7:4] == 4'b0111) ? 1'b1: 1'b0;
assign mova = (instruction[7:2] == 6'b110000) ? 1'b1: 1'b0;
assign movr = (instruction[7:2] == 6'b110001) ? 1'b1: 1'b0;
assign movrhs = (instruction[7:2] == 6'b110010) ? 1'b1: 1'b0;
assign pause = (instruction[7:2] == 6'b111111) ? 1'b1: 1'b0;

endmodule
