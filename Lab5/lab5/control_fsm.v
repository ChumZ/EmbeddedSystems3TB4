module control_fsm (
	input clk, reset_n,
	
	// Status inputs
	input br, brz, addi, subi, sr0, srh0, clr, mov, mova, movr, movrhs, pause,
	input delay_done,
	input temp_is_positive, temp_is_negative, temp_is_zero,
	input register0_is_zero,
	
	// Control signal outputs
	output reg write_reg_file,
	output reg result_mux_select,
	output reg [1:0] op1_mux_select, op2_mux_select,
	output reg start_delay_counter, enable_delay_counter,
	output reg commit_branch, increment_pc,
	output reg alu_add_sub, alu_set_low, alu_set_high,
	output reg load_temp_register, increment_temp_register, decrement_temp_register,
	output reg [1:0] select_immediate,
	output reg [1:0] select_write_address
	
);
parameter RESET=5'b00000, FETCH=5'b00001, DECODE=5'b00010,
			BR=5'b00011, BRZ=5'b00100, ADDI=5'b00101, SUBI=5'b00110, SR0=5'b00111,
			SRH0=5'b01000, CLR=5'b01001, MOV=5'b01010, MOVA=5'b01011,
			MOVR=5'b01100, MOVRHS=5'b01101, PAUSE=5'b01110, MOVR_STAGE2=5'b01111,
			MOVR_DELAY=5'b10000, MOVRHS_STAGE2=5'b10001, MOVRHS_DELAY=5'b10010,
			PAUSE_DELAY=5'b10011;
reg [5:0] state;
reg [5:0] next_state_logic; // NOT REALLY A REGISTER!!!
// State register
always@(posedge clk)
begin
	if(reset_n == 1'b0)
		state <= RESET; 
	else 
		state <= next_state_logic; 
end 
// Output logic
always@(*)
begin 
	case(state) 
		BR: 
		begin 
			write_reg_file = 1'b0; 
			result_mux_select = 1'b0;
			op1_mux_select = 2'b00; //
			op2_mux_select = 2'b01; //
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b1; //
			increment_pc = 1'b0; 
			alu_add_sub = 1'b0;
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b10; //
			select_write_address = 2'b00;
	
		end //br 
		BRZ: 
		begin 
			write_reg_file = 1'b0; 
			result_mux_select = 1'b0;
			op1_mux_select = 2'b00; //
			op2_mux_select = 2'b01; //
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b0; 
			//commit_branch = 1'b0; 
			//increment_pc = 1'b0; 
			alu_add_sub = 1'b0; 
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b10; //
			select_write_address = 2'b00;
			
			if (register0_is_zero == 1'b0)
			begin
				commit_branch = 1'b1; 
				increment_pc = 1'b0;
			end
			else 
			begin
				commit_branch = 1'b0;
				increment_pc = 1'b1; 
			end
		end //brz
		
		SR0: //Might be backwards 
		begin 
			write_reg_file = 1'b1; //
			result_mux_select = 1'b0;//
			op1_mux_select = 2'b11;//
			op2_mux_select = 2'b01;//
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0; 
			increment_pc = 1'b1; //
			alu_add_sub = 1'b0; 
			alu_set_low = 1'b1; //
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b01; //
			select_write_address = 2'b00;//
		end //sr 
		SRH0:  //Might be backwards 
		begin 
			write_reg_file = 1'b1; //
			result_mux_select = 1'b0;//
			op1_mux_select = 2'b11;//
			op2_mux_select = 2'b01;//
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0; 
			increment_pc = 1'b1; //
			alu_add_sub = 1'b0; 
			alu_set_low = 1'b0; 
			alu_set_high = 1'b1;// 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b01; //
			select_write_address = 2'b00;//
		end //srh0 
		
		CLR: 
		begin 
			write_reg_file = 1'b1; //
			result_mux_select = 1'b1; //
			op1_mux_select = 2'b00;
			op2_mux_select = 2'b00;
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0; 
			increment_pc = 1'b1; //
			alu_add_sub = 1'b0; 
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b00; 
			select_write_address = 2'b01; //
		end //clr
		
		MOV: 
		begin 
			write_reg_file = 1'b1;  //
			result_mux_select = 1'b0; //
			op1_mux_select = 2'b01; //
			op2_mux_select = 2'b01; //
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0; 
			increment_pc = 1'b1; //
			alu_add_sub = 1'b0; // 
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b11; //
			select_write_address = 2'b10; //
		end//mov
		
		MOVA: 
		begin 
			write_reg_file = 1'b0; 
			result_mux_select = 1'b0;
			op1_mux_select = 2'b00;
			op2_mux_select = 2'b00;
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0; 
			increment_pc = 1'b1; //
			alu_add_sub = 1'b0; 
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b00; 
			select_write_address = 2'b00;
		end 
		
		MOVR: 
		begin
			write_reg_file = 1'b0; 
			result_mux_select = 1'b0;
			op1_mux_select = 2'b00;
			op2_mux_select = 2'b00;
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0; 
			increment_pc = 1'b0; 
			alu_add_sub = 1'b0; 
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b1; // 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b00; 
			select_write_address = 2'b00;
		end //movr 
		MOVR_STAGE2: 
		begin 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0;  
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			select_immediate = 2'b00; 
			if (temp_is_zero == 1'b1)
			begin 
				increment_pc = 1'b1;
				start_delay_counter = 1'b0;
				decrement_temp_register = 1'b0;
				increment_temp_register = 1'b0;
				op1_mux_select = 2'b00;
				op2_mux_select = 2'b00;
				alu_add_sub = 1'b0;
				result_mux_select = 1'b0;
				select_write_address = 2'b00;
				write_reg_file = 1'b0;
			end 
			else 
			begin 
				increment_pc = 1'b0;
				start_delay_counter = 1'b1; 
				if(temp_is_positive == 1'b1) 
				begin 
				   increment_temp_register = 1'b0;
					decrement_temp_register = 1'b1; 
					op1_mux_select = 2'b10; 
					op2_mux_select = 2'b11; 
					alu_add_sub = 1'b0; 
					result_mux_select = 1'b0; 
					select_write_address = 2'b11;
					write_reg_file = 1'b1; 
				end // if 
				else 
				begin 
					increment_temp_register = 1'b1; 
					decrement_temp_register = 1'b0;
					op1_mux_select = 2'b10; 
					op2_mux_select = 2'b11; 
					alu_add_sub = 1'b1; 
					result_mux_select = 1'b0; 
					select_write_address = 2'b11;
					write_reg_file = 1'b1; 
				end //else 
			end //if 
		end //movr2 
		MOVR_DELAY: 
		begin 
			write_reg_file = 1'b0; 
			result_mux_select = 1'b0;
			op1_mux_select = 2'b00; 
			op2_mux_select = 2'b00; 
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b1; 
			commit_branch = 1'b0; 
			increment_pc = 1'b0; 
			alu_add_sub = 1'b0;
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b00; 
			select_write_address = 2'b00;
		end //movr delay 
		MOVRHS: 
		begin
			write_reg_file = 1'b0; 
			result_mux_select = 1'b0;
			op1_mux_select = 2'b00; 
			op2_mux_select = 2'b00; 
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0;
			increment_pc = 1'b0; 
			alu_add_sub = 1'b0;
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b1; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b00; 
			select_write_address = 2'b00;	
		end //movr 
		MOVRHS_STAGE2: 
		begin
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0;  
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			select_immediate = 2'b00; 
			if (temp_is_zero == 1'b1)
			begin
				increment_pc = 1'b1;
				start_delay_counter = 1'b0;
				decrement_temp_register = 1'b0;
				increment_temp_register = 1'b0;
				op1_mux_select = 2'b00;
				op2_mux_select = 2'b00;
				alu_add_sub = 1'b0;
				result_mux_select = 1'b0;
				select_write_address = 2'b00;
				write_reg_file = 1'b0;
			end
			else 
			begin
				start_delay_counter = 1'b1; 
				if(temp_is_positive > 0) 
				begin 
					increment_temp_register = 1'b0;
					decrement_temp_register = 1'b1; 
					op1_mux_select = 2'b10; 
					op2_mux_select = 2'b10; 
					alu_add_sub = 1'b0; 
					result_mux_select = 1'b0; 
					select_write_address = 2'b11;
					write_reg_file = 1'b1; 
				end // if 
				else 
				begin
					increment_temp_register = 1'b1; 
					decrement_temp_register = 1'b0;
					op1_mux_select = 2'b10; 
					op2_mux_select = 2'b10; 
					alu_add_sub = 1'b1; 
					result_mux_select = 1'b0; 
					select_write_address = 2'b11;
					write_reg_file = 1'b1;
				end //ele
			end
			end //movr2 
		MOVRHS_DELAY: 
		begin 
			write_reg_file = 1'b0; 
			result_mux_select = 1'b0;
			op1_mux_select = 2'b00;
			op2_mux_select = 2'b00;
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b1;  //
			commit_branch = 1'b0; 
			increment_pc = 1'b0;
			alu_add_sub = 1'b0; 
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b00; 
			select_write_address = 2'b00; 
		end //movr delay 
		
		ADDI: 
		begin 
			write_reg_file = 1'b1;   //
			result_mux_select = 1'b0; //
			op1_mux_select = 2'b01; //
			op2_mux_select = 2'b01; //
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0; 
			increment_pc = 1'b1; //
			alu_add_sub = 1'b0; //
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b00; //
			select_write_address = 2'b01; //
		end //addi 
		
		SUBI: 
		begin 
			write_reg_file = 1'b1; //
			result_mux_select = 1'b0; //
			op1_mux_select = 2'b01; //
			op2_mux_select = 2'b01; //
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0; 
			increment_pc = 1'b1; //
			alu_add_sub = 1'b1; //
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b00; //
			select_write_address = 2'b01; //
		end //subi 
		
		PAUSE: 
		begin 
			write_reg_file = 1'b0; 
			result_mux_select = 1'b0;
			op1_mux_select = 2'b00;
			op2_mux_select = 2'b00;
			start_delay_counter = 1'b1; 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0; 
			increment_pc = 1'b0;
			alu_add_sub = 1'b0; 
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b00; 
			select_write_address = 2'b00; 
		end 
		
		PAUSE_DELAY:
		begin 
			write_reg_file = 1'b0; 
			result_mux_select = 1'b0;
			op1_mux_select = 2'b00;
			op2_mux_select = 2'b00;
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b1;  // 
			commit_branch = 1'b0; 
			alu_add_sub = 1'b0; 
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b00; 
			select_write_address = 2'b00; 
			
			if(delay_done) 
				increment_pc = 1'b1; 
			else
				increment_pc = 1'b0;
		end 
		
		default: 
		begin 
			write_reg_file = 1'b0; 
			result_mux_select = 1'b0;
			op1_mux_select = 2'b00;
			op2_mux_select = 2'b00;
			start_delay_counter = 1'b0; 
			enable_delay_counter = 1'b0; 
			commit_branch = 1'b0; 
			increment_pc = 1'b0; 
			alu_add_sub = 1'b0; 
			alu_set_low = 1'b0; 
			alu_set_high = 1'b0; 
			load_temp_register = 1'b0; 
			increment_temp_register = 1'b0; 
			decrement_temp_register = 1'b0; 
			select_immediate = 2'b00; 
			select_write_address = 2'b00;
		end 
	endcase 
end 
// Next state logic
always@(*)
begin 
	case(state)
		RESET : 
		begin 
			next_state_logic = FETCH; 
		end // Reset 
		FETCH :
		begin 
			next_state_logic = DECODE; 
		end // Fetch 
		DECODE: 
		begin 
			if(br) 
			begin 
				next_state_logic = BR; 
			end 
			else if (addi)
			begin 
				next_state_logic = ADDI; 
			end 
			else if (subi)
			begin 
				next_state_logic = SUBI; 
			end 
			else if (mov)
			begin 
				next_state_logic = MOV; 
			end 
			else if (sr0)
			begin 
				next_state_logic = SR0; 
			end 
			else if (srh0)
			begin 
				next_state_logic = SRH0; 
			end 
			else if (clr)
			begin 
				next_state_logic = CLR; 
			end 
			else if (brz)
			begin 
				next_state_logic = BRZ; 
			end 
			else if (movr)
			begin 
				next_state_logic = MOVR; 
			end 
			else if (movrhs)
			begin 
				next_state_logic = MOVRHS; 
			end 
			else if (mova)
			begin 
				next_state_logic = MOVA; 
			end 
			else if (pause)
			begin 
				next_state_logic = PAUSE; 
			end 
		end //decode 
		BR: 
		begin 
			next_state_logic = FETCH; 
		end //br 
		BRZ:
		begin 
			next_state_logic = FETCH; 
		end //brz
		ADDI: 
		begin 
			next_state_logic = FETCH; 
		end //addi 
		SUBI: 
		begin 
			next_state_logic = FETCH; 
		end //subi 
		SR0: 
		begin 
			next_state_logic = FETCH; 
		end //sr0 
		SRH0: 
		begin 
			next_state_logic = FETCH; 
		end //srh0 
		CLR: 
		begin 
			next_state_logic = FETCH; 
		end //clr
		MOV:
		begin 
			next_state_logic = FETCH; 
		end //mov 
		MOVA:
		begin 
			next_state_logic = FETCH; 
		end //mova 
		MOVR: 
		begin 
			next_state_logic = MOVR_STAGE2; 
		end // movr 
		MOVR_STAGE2: 
		begin 
			if(temp_is_zero) 
				next_state_logic = FETCH;
			else 
				next_state_logic = MOVR_DELAY; 		
		end //movr2 
		MOVR_DELAY: 
		begin 
			if(delay_done) 
				next_state_logic = MOVR_STAGE2; 
			else 
				next_state_logic = MOVR_DELAY; 
		end //movr delay
		MOVRHS:
		begin 
			next_state_logic = MOVRHS_STAGE2;
		end //movrhs 
		MOVRHS_STAGE2: 
		begin 
			if(temp_is_zero) 
				next_state_logic = FETCH;
			else 
				next_state_logic = MOVRHS_DELAY; 	
		end //movrhs 
		MOVRHS_DELAY:
		begin 
			if(delay_done) 
				next_state_logic = MOVRHS_STAGE2; 
			else 
				next_state_logic = MOVRHS_DELAY; 
		end //movrhs delay 
		PAUSE: 
		begin 
			next_state_logic = PAUSE_DELAY; 
		end //pause 
		PAUSE_DELAY: 	
		begin 
			if(delay_done) 
				next_state_logic = FETCH; 
			else 
				next_state_logic = PAUSE_DELAY; 
		end //pause delay 
		default: 
		begin 
			next_state_logic = RESET; 
		end //defualt 
	endcase
end
endmodule