module temp_register (input clk, reset_n, load, increment, decrement, input [7:0] data,
					output negative, positive, zero);
reg [7:0] counter;
					
					
always @(posedge clk)
begin
	if(!reset_n)
	begin
		counter<= 8'h00;
	end
	else if(load)
	begin
		counter<= data;
	end
	else
	begin
		if(increment)
		begin
			counter<= counter + 8'h01;
		end
		if(decrement)
		begin
			counter<= counter - 8'h01;
		end
	end
end

assign positive = (counter>0) ? 1'b1: 1'b0;
assign negative = (counter<0) ? 1'b1: 1'b0;
assign zero = (counter==0) ? 1'b1: 1'b0;

endmodule
