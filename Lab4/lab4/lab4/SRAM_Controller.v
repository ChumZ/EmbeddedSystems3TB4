/*****************************************************************************
 *                                                                           *
 * Module:       SRAM_Controller                                             *
 * Description:                                                              *
 *      This module is used for the sram controller for 3TB4 lab 4           *
 *                                                                           *
 *****************************************************************************/

module SRAM_Controller (
input           clk,
input		[17:0]address,
input				chipselect,
input		[1:0]	byte_enable, 
input				read, 
input				write, 
input resetn,  // unused but needed for Qsys later not sure if it needs to be reset_n
input		[15:0]write_data,


// Bidirectionals
inout		[15:0]	SRAM_DQ,

// Outputs
output		[15:0]read_data,
output		[17:0]	SRAM_ADDR,
output				SRAM_CE_N,
output				SRAM_WE_N,
output				SRAM_OE_N,
output				SRAM_UB_N,
output				SRAM_LB_N
);

// Add your code here
assign SRAM_CE_N = chipselect ? 0  : 1 ; 
assign SRAM_WE_N = write ? 0 : 1; 
assign SRAM_OE_N = read ? 0 : 1; 
assign SRAM_UB_N = byte_enable[1] ? 0 : 1; 
assign SRAM_LB_N = byte_enable[0] ? 0 : 1; 

assign SRAM_ADDR = address; 
assign SRAM_DQ = (read) ? read_data : (write) ? write_data :  16'hzzzz;   

endmodule

