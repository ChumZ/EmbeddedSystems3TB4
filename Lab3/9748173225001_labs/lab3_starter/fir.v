module fir (input clock, input signed [15:0] sig_in, input reset, output reg signed[15:0] sig_out);
	reg signed [15:0] coeff [30:0];
	wire signed [31:0] add [N-1:0];
   reg signed [15:0] held [N-1:0]; 
	reg signed [31:0] sum; 
	
	parameter N = 31;
	
	always @(*)
	begin
		coeff[  0]=         0;
		coeff[  1]=       756;
		coeff[  2]=        -0;
		coeff[  3]=     -1010;
		coeff[  4]=         0;
		coeff[  5]=      1540;
		coeff[  6]=        -0;
		coeff[  7]=     -2112;
		coeff[  8]=        -0;
		coeff[  9]=      2662;
		coeff[ 10]=         0;
		coeff[ 11]=     -3119;
		coeff[ 12]=         0;
		coeff[ 13]=      3422;
		coeff[ 14]=        -0;
		coeff[ 15]=     29240;
		coeff[ 16]=        -0;
		coeff[ 17]=      3422;
		coeff[ 18]=         0;
		coeff[ 19]=     -3119;
		coeff[ 20]=         0;
		coeff[ 21]=      2662;
		coeff[ 22]=        -0;
		coeff[ 23]=     -2112;
		coeff[ 24]=        -0;
		coeff[ 25]=      1540;
		coeff[ 26]=         0;
		coeff[ 27]=     -1010;
		coeff[ 28]=        -0;
		coeff[ 29]=       756;
		coeff[ 30]=         0;
	end
	
	genvar i; 
	generate
		for (i=0; i < N; i=i+1)
		begin: multipliers
				multiplier m (.dataa(coeff[i]), .datab(held[i]), .result(add[i]));
		end
	endgenerate	
	
	integer k; 
	always@(posedge clock or posedge reset)
	begin 
		if(reset)
		begin 	
			for (k = 0; k < N; k = k + 1)
			begin 
				held[k] <= 0; 
			end 
			//sig_out <= 0; 
		end 
		else 
		begin 
			for (k = N - 1; k > 0; k = k - 1)
			begin 
				held[k] <= held[k-1]; 
			end 
			held[0] <= sig_in; 
		end 
	end 
	
	always@(*) 
	begin 
		integer j; 
		sum = 0; 
		for(j =0; j < N; j = j + 1)
		begin 
			sum = sum + add[j]; 
		end 
		
		sum = sum >> 15; 
		sig_out <= sum[15:0];
	end 
endmodule  