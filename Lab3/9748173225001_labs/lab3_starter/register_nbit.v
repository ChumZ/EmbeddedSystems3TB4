module register_nbit (input [N-1:0] d, input clk, input resetn, output reg [N-1:0] Q); 
	parameter N = 32; 
	
	always @(posedge clk or negedge resetn) 
	begin 
		if (resetn == 1’b0) 
			Q <= {N{1’b0}}; 
		else Q <= d; 
	end 
endmodule 