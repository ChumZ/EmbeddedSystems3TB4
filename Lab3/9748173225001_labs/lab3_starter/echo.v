module echo (input clock, reset, input signed [15:0] sig_in, inout reg signed [15:0] sig_out); 
	wire [15:0] sig_delay, sig_attdelay;  
	
	shiftreg (.clock(clock), .shiftin(sig_out), .shiftout(sig_delay)); 
	div2 (.a(sig_delay), .b(sig_attdelay)); 

	always@(*) 
	begin 
			sig_out <= sig_attdelay + sig_in; 
	end 
endmodule 

module div2 (input signed [15:0] a, output signed[15:0] b); 
	assign b = a >> 4; 
endmodule 