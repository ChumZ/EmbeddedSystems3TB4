module dsp_subsystem (input sample_clock,  input reset, input [1:0] selector, input signed [15:0] input_sample, output signed[15:0] output_sample);
	wire signed[15:0] sample, fir_sample, echo_sample; 
	
	assign sample = input_sample;
	fir(.clock(sample_clock), .sig_in(input_sample), .sig_out(fir_sample), .reset(reset)); 
	echo(.clock(sample_clock), .sig_in(input_sample), .sig_out(echo_sample), .reset(reset)); 
	
	mux3to1(.sel(selector), .in1(sample), .in2(fir_sample), .in3(echo_sample), .out(output_sample), ); 
	
endmodule
