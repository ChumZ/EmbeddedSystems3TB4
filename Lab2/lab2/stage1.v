module stage1 (input CLOCK_50, input [3:0] SW, output wire [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7);
	seven_seg_decoder(.x(SW),.hex_LEDs(HEX0));
	seven_seg_decoder(.x(SW),.hex_LEDs(HEX1));
	seven_seg_decoder(.x(SW),.hex_LEDs(HEX2));
	seven_seg_decoder(.x(SW),.hex_LEDs(HEX3));
	seven_seg_decoder(.x(SW),.hex_LEDs(HEX4));
	seven_seg_decoder(.x(SW),.hex_LEDs(HEX5));
	seven_seg_decoder(.x(SW),.hex_LEDs(HEX6));
	seven_seg_decoder(.x(SW),.hex_LEDs(HEX7));
endmodule

module seven_seg_decoder (input [3:0] x, output[6:0] hex_LEDs);
reg [6:0] top_7_segments;

assign hex_LEDs[6:0] = top_7_segments[6:0];
always @(x)
  case(x)
    4'h0:top_7_segments = 7'b1000000;
	 4'h1:top_7_segments = 7'b1111001;
	 4'h2:top_7_segments = 7'b0100100;
	 4'h3:top_7_segments = 7'b0110000;
	 4'h4:top_7_segments = 7'b0011001;
	 4'h5:top_7_segments = 7'b0010010;
	 4'h6:top_7_segments = 7'b0000010;
	 4'h7:top_7_segments = 7'b1111000;
	 4'h8:top_7_segments = 7'b0000000;
	 4'h9:top_7_segments = 7'b0010000;
	 4'ha:top_7_segments = 7'b0001000;
	 4'hb:top_7_segments = 7'b0000011;
	 4'hc:top_7_segments = 7'b1000110;
	 4'hd:top_7_segments = 7'b0100001;
	 4'he:top_7_segments = 7'b0000110;
	 4'hf:top_7_segments = 7'b0001110;
	 default: top_7_segments = 7'bx;
  endcase
endmodule