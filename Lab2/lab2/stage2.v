module stage2 (input CLOCK_50, input [3:0] SW, output wire [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7);
	wire [3:0] led0, led1, led2, led3, led4, led5, led6, led7;
	
	hex_to_BCD_converter(.clk(CLOCK_50), .hex_number(SW), .bcd_digit_0(led0), .bcd_digit_1(led1), .bcd_digit_2(led2), .bcd_digit_3(led3), .bcd_digit_4(led4), .bcd_digit_5(led5), .bcd_digit_6(led6), .bcd_digit_7(led7));
	seven_seg_decoder(.x(led0),.hex_LEDs(HEX0));
	seven_seg_decoder(.x(led0),.hex_LEDs(HEX0));
	seven_seg_decoder(.x(led1),.hex_LEDs(HEX1));
	seven_seg_decoder(.x(led2),.hex_LEDs(HEX2));
	seven_seg_decoder(.x(led3),.hex_LEDs(HEX3));
	seven_seg_decoder(.x(led4),.hex_LEDs(HEX4));
	seven_seg_decoder(.x(led5),.hex_LEDs(HEX5));
	seven_seg_decoder(.x(led6),.hex_LEDs(HEX6));
	seven_seg_decoder(.x(led7),.hex_LEDs(HEX7));
endmodule

//Hex to BCD Converter 
module hex_to_BCD_converter(clk, Reset, hex_number,bcd_digit_0,bcd_digit_1,bcd_digit_2,
	bcd_digit_3,bcd_digit_4,bcd_digit_5, bcd_digit_6, bcd_digit_7);
	input wire clk, Reset; 
	input wire [17:0] hex_number; 
	output reg [3:0] bcd_digit_0, bcd_digit_1, bcd_digit_2, bcd_digit_3,
	bcd_digit_4, bcd_digit_5, bcd_digit_6, bcd_digit_7;
   
	integer i; 
	always@(hex_number)
	begin
		//Set digits to zero 
		bcd_digit_0 = 4'b0000; 
		bcd_digit_1 = 4'b0000;
		bcd_digit_2 = 4'b0000;
		bcd_digit_3 = 4'b0000;
		bcd_digit_4 = 4'b0000;
		bcd_digit_5 = 4'b0000;
		bcd_digit_6 = 4'b0000;
		bcd_digit_7 = 4'b0000;
		
		for (i=17; i>=0 ; i=i-1)
		begin 
			//add 3 to columns 
			if (bcd_digit_7 >= 5)
				bcd_digit_7 = bcd_digit_7 + 4'b0011; 
			if (bcd_digit_6 >= 5)
				bcd_digit_6 = bcd_digit_6 + 4'b0011; 
			if (bcd_digit_5 >= 5)
				bcd_digit_5 = bcd_digit_5 + 4'b0011; 
			if (bcd_digit_4 >= 5)
				bcd_digit_4 = bcd_digit_4 + 4'b0011; 
			if (bcd_digit_3 >= 5)
				bcd_digit_3 = bcd_digit_3 + 4'b0011; 
			if (bcd_digit_2 >= 5)
				bcd_digit_2 = bcd_digit_2 + 4'b0011; 
			if (bcd_digit_1 >= 5)
				bcd_digit_1 = bcd_digit_1 + 4'b0011;
			if (bcd_digit_0 >= 5)
				bcd_digit_0 = bcd_digit_0 + 4'b0011; 
				
			//Shift Left 
			bcd_digit_7 = bcd_digit_7 << 1; 
			bcd_digit_7[0] = bcd_digit_6[3]; 
			bcd_digit_6 = bcd_digit_6 << 1; 
			bcd_digit_6[0] = bcd_digit_5[3]; 
			bcd_digit_5 = bcd_digit_5 << 1; 
			bcd_digit_5[0] = bcd_digit_4[3]; 
			bcd_digit_4 = bcd_digit_4 << 1; 
			bcd_digit_4[0] = bcd_digit_3[3]; 
			bcd_digit_3 = bcd_digit_3 << 1; 
			bcd_digit_3[0] = bcd_digit_2[3]; 
			bcd_digit_2 = bcd_digit_2 << 1; 
			bcd_digit_2[0] = bcd_digit_1[3]; 
			bcd_digit_1 = bcd_digit_1 << 1; 
			bcd_digit_1[0] = bcd_digit_0[3]; 
			bcd_digit_0 = bcd_digit_0 << 1; 
			bcd_digit_0[0] = hex_number[i]; 
		end 
	end
endmodule 


module seven_seg_decoder (input [3:0] x, output[6:0] hex_LEDs);
reg [6:0] top_7_segments;

assign hex_LEDs[6:0] = top_7_segments[6:0];
always @(x)
  case(x)
    4'h0:top_7_segments = 7'b1000000;
	 4'h1:top_7_segments = 7'b1111001;
	 4'h2:top_7_segments = 7'b0100100;
	 4'h3:top_7_segments = 7'b0110000;
	 4'h4:top_7_segments = 7'b0011001;
	 4'h5:top_7_segments = 7'b0010010;
	 4'h6:top_7_segments = 7'b0000010;
	 4'h7:top_7_segments = 7'b1111000;
	 4'h8:top_7_segments = 7'b0000000;
	 4'h9:top_7_segments = 7'b0010000;
	 4'ha:top_7_segments = 7'b0001000;
	 4'hb:top_7_segments = 7'b0000011;
	 4'hc:top_7_segments = 7'b1000110;
	 4'hd:top_7_segments = 7'b0100001;
	 4'he:top_7_segments = 7'b0000110;
	 4'hf:top_7_segments = 7'b0001110;
	 default: top_7_segments = 7'bx;
  endcase
endmodule